let express = require('express')
let fs = require('fs')
let app = express()



app.get('/', (req, res) => {
  fs.readFile('./MOCK_DATA.json', (err, data) => {
    if(err) {
      console.log(err) 
    } else {
      res.send(data)
    }
  })
})

app.use(express.static('pic'))


app.listen('3000', ()=> {console.log('port 3000')})
